# serial-utility

Network accessible system serial utility that can interface with serial devices. 

Either subclass from here or compose this to implement other devices which need serial communication, or
use it directly to issue instructions to your device. 

pip installable.

# To use

run executor.py script directly

#### Properties/Parameters

* read_timeout
* write_timeout
* baud_rate
* comport
* byte_size
* parity
* stopbits
* x-on-x-off
* rtscts
* dsrstr

#### Actions

* connect
* disconnect
* execution (instruction)