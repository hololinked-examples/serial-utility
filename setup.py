import setuptools

setuptools.setup(
    name="serial_utility",
    version="0.1.0",
    author="Vignesh Vaidyanathan",
    author_email="vignesh.vaidyanathan@hololinked.dev",
    description="network compatible pyserial",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hololinked-examples/serial-utility",
    packages=['serial_utility'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],    
    requires=['pyserial'],
    python_requires='>=3.7',
)
 