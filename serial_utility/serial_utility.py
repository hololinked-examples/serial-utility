import logging
import serial
from enum import StrEnum
from threading import RLock

from hololinked.server import Thing as RemoteObject, StateMachine, action, HTTP_METHODS
from hololinked.server.properties import Number, Selector, String, Boolean



class SerialCommunication(RemoteObject):
    """
    System serial utility that can interface with serial devices. Either subclass from here to implement other devices 
    which need serial communication, or use it directly to issue instructions to your device. 
    """

    read_timeout = Number(default=0.1, bounds=(0,None), allow_None=True, 
                        doc="maximum time to wait until read is complete, set None (null) for indefinite, unit - seconds.", 
                        db_persist=True, URL_path='/read-timeout')
    write_timeout = Number(default=100, bounds=(0,None), allow_None=True,
                        doc="maximum time to wait until write is complete, set None (null) for indefinite, unit - seconds.", 
                        db_persist=True, URL_path='/write-timeout')
    baud_rate = Selector(objects=[9600, 14400, 19200, 38400, 57600, 115200,
                            128000, 256000, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 
                            1500000, 2000000, 2500000, 3000000, 3500000, 4000000], default=9600, 
                    doc="allowed values are 9600, 14400, 19200, 38400, 57600, 115200, 128000, 256000...until 4000000",  
                    db_persist=True, URL_path='/baud-rate')
    url = String(doc="serial port URL depending on operating system. e.g. /dev/ttyUSB0 on GNU/Linux or COM3 on Windows",
                    default='COM100', db_persist=True, URL_path='/url', 
                    regex=r"^(COM[1-9][0-9]{0,2}|/dev/tty(?:S|USB)[0-9]+|[a-zA-Z0-9.-]+:[0-9]{1,5})$")
    byte_size = Selector(default=serial.EIGHTBITS, objects=[serial.EIGHTBITS, serial.SEVENBITS, serial.SIXBITS, serial.FIVEBITS], 
                        doc="Number of data bits. Possible values: 5, 6, 7, 8", 
                        db_persist=True, URL_path='/byte-size')
    parity = Selector(default=serial.PARITY_NONE, objects=[serial.PARITY_NONE, serial.PARITY_EVEN, serial.PARITY_ODD, 
                                                serial.PARITY_MARK, serial.PARITY_SPACE], 
                doc=f"""partiy, Possible values are none(int value {serial.PARITY_NONE}), even({serial.PARITY_EVEN}), odd({serial.PARITY_ODD}), 
                    mark({serial.PARITY_MARK})""", db_persist=True, URL_path='/parity')
    stopbits = Selector(default=serial.STOPBITS_ONE, objects=[serial.STOPBITS_ONE, serial.STOPBITS_TWO, serial.STOPBITS_ONE_POINT_FIVE],
                    doc="stopbits", db_persist=True, URL_path='/connection/stopbits')
    rtscts = Boolean(default=False, doc="enable/disable hardware (RTS/CTS) flow control.", db_persist=True, 
                        URL_path='/rtscts')
    dsrdtr = Boolean(default=False, doc="enable/disable hardware (DSR/DTR) flow control.", db_persist=True, 
                        URL_path='/dsrdtr')
    inter_byte_timeout = Number(default=None, allow_None=True, bounds=(0, None), db_persist=True, 
                            doc="Inter-character timeout, None to disable (default).",
                            URL_path='/inter-byte-timeout')
    xonxoff = Boolean(default=False, doc="xonoff", db_persist=True, URL_path='/xonxoff')
    

    def __init__(self, instance_name : str, serial_url = "COM100", read_timeout=0.1, write_timeout=0.1, baud_rate=9600, 
                byte_size=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=False, 
                **kwargs) -> None:
        super().__init__(
            instance_name=instance_name,
            url=serial_url,
            read_timeout=read_timeout,
            write_timeout=write_timeout, 
            baud_rate=baud_rate, 
            byte_size=byte_size,
            parity=parity,
            stopbits=stopbits,
            xonxoff=xonxoff,
            **kwargs      
        )
        self._r_lock = RLock()

    @action(URL_path='/connect', http_method=HTTP_METHODS.POST) 
    def connect(self):
        """
        connects to the serial port with given (defined) parameters.
        Returns None upon opening COM port, otherwise raises SerialException.
        """     
        self.logger.debug("Connecting to {} with baudrate : {}, read timeout : {}, bytesize : {}, stopbits : {}, xonxoff = {}, write timeout : {}".format( 
             self.url, self.baud_rate, self.read_timeout, self.byte_size, self.stopbits, self.xonxoff, self.write_timeout))    
        self._device = serial.Serial(port=self.url, baudrate=self.baud_rate, timeout=self.read_timeout, 
                bytesize=self.byte_size, stopbits=self.stopbits, xonxoff=self.xonxoff, rtscts=self.rtscts,
                dsrdtr=self.dsrdtr, write_timeout=self.write_timeout)
        if self._device.is_open:      
            self.state_machine.set_state(self.states.ON)                      
           
    @action(URL_path="/disconnect", http_method=HTTP_METHODS.POST)
    def disconnect(self):
        """
        disconnects from port. 
        """           
        self._device.close()
        self.state_machine.set_state(self.states.DISCONNECTED)       
                
    @action(URL_path="/execute", http_method=HTTP_METHODS.POST, input_schema={"command": "string", "return_data_size": "integer"})
    def execute_instruction(self, command : str, return_data_size : int = 0):
        """
        executes instruction given by the ASCII string parameter 'command'.q
        If return data size is greater than 0, it reads the response and returns the response. 
        Return Data Size - in bytes - 1 ASCII character = 1 Byte.		
        """
        try:
            if 'instructions' in self.properties and command not in self.properties["instructions"]:
                raise RuntimeError(f"command {command} not a valid command.")
            self._r_lock.acquire()
            bytes_data = None
            command = command.encode(encoding='ascii', errors='strict') # type: bytes
            self.logger.debug("Issuing command {} to {}".format(command, self.url)) 
            self.state_machine.set_state(self.states.COMMUNICATING) # useful for higher timeout instructions
            self._device.reset_input_buffer()
            self._device.reset_output_buffer()
            self.logger.debug("number of bytes written : {}".format(self._device.write(command)))# type: ignore
            if(return_data_size > 0):
                bytes_data = self._device.read(return_data_size)            
                self._device.reset_input_buffer()
                self._device.reset_output_buffer()
                bytes_data = bytes_data.decode("ascii")
                self.logger.debug("received reply {} from {}".format(bytes_data, self.url))
            self.state_machine.set_state(self.states.ON)    
            return bytes_data
        except:
            raise 
        finally:
            self._r_lock.release()
            if self._device.is_open:
                self.state_machine.set_state(self.states.ON)    
            else:
                self.state_machine.set_state(self.states.FAULT)
        
    @action(URL_path='/reset-fault', http_method=HTTP_METHODS.POST) # endpoint could be improved
    def reset_fault(self):
        if self._device.is_open:
            self.state_machine.set_state(self.states.ON)    
            return
        try:
            self.connect()
        except Exception as ex:
            self.state_machine.set_state(self.states.DISCONNECTED)
            raise ex from None # still raise exception
      

    class states(StrEnum):
        DISCONNECTED = "DISCONNECTED"
        ON = "ON"
        COMMUNICATING = "COMMUNICATING"
        FAULT = "FAULT"

    state_machine = StateMachine(
        states=states,
        initial_state=states.DISCONNECTED,
        ON=[execute_instruction, disconnect], 
        DISCONNECTED=[connect, read_timeout, write_timeout, xonxoff, byte_size, 
                    baud_rate, url, parity, stopbits],
    )
   
        

def local_device_example():
    dev = SerialCommunication(instance_name='system-serial-utility', 
            log_level=logging.DEBUG, url='COM9', read_timeout=0.1, 
            write_timeout=0.1, xonxoff=False, baud_rate=115200)
    dev.connect()
    print(dev.execute_instruction("*VER", 100))
    dev.disconnect()


if __name__ == "__main__":
    local_device_example()
  