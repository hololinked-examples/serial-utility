import ssl
import os
import logging
from multiprocessing import Process
from hololinked.server import HTTPServer
from serial_utility import SerialCommunication



def start_https_server():
    ssl_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain(f'assets{os.sep}security{os.sep}certificate.pem',
                        keyfile=f'assets{os.sep}security{os.sep}key.pem')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_3

    H = HTTPServer(things=['system-serial-utility'], 
                    port=8085, log_level=logging.DEBUG,  ssl_context=ssl_context)
    H.listen()


def start_http_server():
    H = HTTPServer(['system-serial-utility'], port=8085,
                      log_level=logging.DEBUG)  
    H.listen()


def run():
    P = Process(target=start_https_server)
    P.start()
    
    S = SerialCommunication(instance_name='system-serial-utility')
    S.run() 


if __name__ == '__main__':
    run()

